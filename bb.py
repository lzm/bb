"""
Banco do Brasil library
Copyright (C) 2013  Lessandro Z. Mariano

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import decimal
import lxml.html
import re
import requests
import json


class BBClient(object):
    """
    Python client for BB's mobile website

    Usage:
        client = BBClient()
        client.login('0000-0', '00000-0', '00000000')
        jan = client.get_extract(1)
    """

    URL = 'https://mobi.bb.com.br/iphone/'

    def __init__(self):
        self.session = requests.Session()
        self.session.headers['User-Agent'] = 'android 2'

    def login(self, ag, cc, pw):
        """
        Logs in BB's mobile site

        Params:
            ag: '0000-0'
            cc: '00000-0'
            pw: '00000000'
        """
        self.session.cookies.clear()

        r = self.session.post(self.URL)
        action = re.search('bb.contr.*Login', r.content).group(0)

        data = {
            'titular': '01',
            'dependenciaOrigem': ag,
            'numeroContratoOrigem': cc,
            'senhaConta': pw,
            'botaoOk': 'Entrar',
            'idh': '',
            'iphm': '',
            'hash': '',
            'apld': ''
        }

        self.session.post(self.URL + action, data=data)

    def load_cookies(self, filename):
        """
        Loads session cookies from a file
        """

        with open(filename, 'r') as fp:
            cookies = json.load(fp)
            for k, v in cookies.iteritems():
                self.session.cookies[k] = v

    def save_cookies(self, filename):
        """
        Saves session cookies to a file
        """

        with open(filename, 'w') as fp:
            cookies = self.session.cookies.get_dict()
            json.dump(cookies, fp, indent=4, separators=(',', ': '))

    def get_extract(self, month=None):
        """
        Gets the extract for a specific month
        If month is not specified, gets the extract for the current month

        Returns a list of tuples: (day, description, detail, amount)
        """

        data = {
            'rbPeriodo': '1',
            'dia': '1',
            'mes': '1',
            'botaoOk': 'Ok'
        }

        if month is not None:
            data['rbPeriodo'] = '2'
            data['mes'] = str(month)

        r = self.session.post(self.URL + 'bb.contr?tr=009', data)

        return list(self.parse_extract_html(r.content))

    def parse_extract_html(self, raw_html):
        parsed = lxml.html.fromstring(raw_html)
        table = parsed.xpath('//table')[0]
        rows = table.xpath('./tr[position()>2]')

        day = 0
        for row in rows:
            if row[0].text.strip():
                day = int(row[0].text)
            description = row[1].text.strip()
            detail = ''
            if len(row[1]) > 0:
                detail = row[1][1].text.strip()
            amount_text = row[2][0].text
            amount = decimal.Decimal(re.sub(r'[^0-9]', '', amount_text)) / 100
            if 'D' in amount_text:
                amount *= -1

            yield (day, description, detail, amount)
